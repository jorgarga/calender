use std::env;
use std::process;

mod calender;
use crate::calender::calender::*;

fn validate_year(year: usize) {
    if year < 1700 {
        println!("Too far into the past. Try another date!");
        process::exit(0);
    }
}

fn print_calender(year: usize) {
    validate_year(year);
    calender_for_year(year);
}

fn print_month(month: usize, year: usize) {
    validate_year(year);
    calender_for_month(month - 1, year);
}

fn print_help() {
    println!("Usage:\ncalender year\ncalender month year");
    println!();
    println!("Examples");
    println!("calender 1999    # prints the calender for year 1999");
    println!("calender 12 1999 # prints the calender for december 1999");
}

fn main() {
    let args: Vec<String> = env::args().collect();

    match args.len() {
        2 => {
            let year: usize = args[1].parse::<usize>().unwrap();
            print_calender(year);
        }
        3 => {
            let month: usize = args[1].parse::<usize>().unwrap();
            let year: usize = args[2].parse::<usize>().unwrap();
            print_month(month, year);
        }
        _ => {
            print_help();
        }
    }
}
