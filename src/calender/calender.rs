pub fn calender_for_year(year: usize) {
    let delta = delta_year(year);
    let is_leap = is_leap_year(year);
    let months = get_month_names();
    let days = get_days_in_months(is_leap);

    yearly_calender(delta, days, months);
}

pub fn calender_for_month(month: usize, year: usize) {
    let is_leap = is_leap_year(year);
    let months = get_month_names();
    let days = get_days_in_months(is_leap);
    let delta = delta_month(month, delta_year(year), days);

    month_calender(months[month], days[month], delta);
}

fn is_leap_year(year: usize) -> bool {
    // One way of doing it with matching:
    // match (year % 400, year % 100, year % 4) {
    //      (0, _, _) => true,
    //      (_, 0, _) => false,
    //      (_, _, 0) => true,
    //      (_, _, _) => false
    // }
    //
    // But lets use the classic one: I believe it might
    // be slightly faster.
    (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0)
}

fn delta_year(year: usize) -> usize {
    let year_minus_1 = year - 1;

    let by4 = year_minus_1 / 4;
    let by100 = year_minus_1 / 100;
    let by400 = year_minus_1 / 400;

    (year + by4 - by100 + by400 - 1) % 7
}

fn delta_month(month: usize, delta_year: usize, days_in_months: [usize; 12]) -> usize {
    if month == 0 {
        return delta_year;
    }

    let mut days_to_add = delta_year;
    for (index, days) in days_in_months.iter().enumerate() {
        if index > month - 1 {
            break;
        }

        days_to_add += days
    }

    days_to_add % 7
}

fn get_days_in_months(is_leap_year: bool) -> [usize; 12] {
    match is_leap_year {
        true => [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        false => [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
    }
}

fn get_month_names() -> [&'static str; 12] {
    [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ]
}

fn full_space(mut space: usize) {
    while space > 0 {
        print!(" ");
        space -= 1;
    }
}

fn month_calender(name: &str, days: usize, delta: usize) {
    println!("\n {}", name);
    println!("Mon  Tue  Wed  Thu  Fri  Sat  Sun\n");
    full_space(delta * 5);

    for day in 1..=days {
        print!("{:2}", day);

        if (day + delta) % 7 > 0 {
            print!("   ");
        } else {
            println!();
        }
    }
    println!("\n");
}

fn yearly_calender(mut delta: usize, days_in_months: [usize; 12], months: [&str; 12]) {
    for (index, month) in months.iter().enumerate() {
        month_calender(month, days_in_months[index], delta);

        delta = (days_in_months[index] + delta) % 7;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_delta_month_november_2021() {
        let month = 10; // November
        let delta_year = delta_year(2021);
        let days_in_months = get_days_in_months(false);
        let result = delta_month(month, delta_year, days_in_months);
        assert_eq!(result, 0);
    }

    #[test]
    fn test_delta_month_december_2021() {
        let month = 11; // December
        let delta_year = delta_year(2021);
        let days_in_months = get_days_in_months(false);
        let result = delta_month(month, delta_year, days_in_months);
        assert_eq!(result, 2);
    }

    #[test]
    fn test_delta_month_january_2022() {
        let month = 0; // january
        let delta_year = delta_year(2022);
        let days_in_months = get_days_in_months(false);
        let result = delta_month(month, delta_year, days_in_months);
        assert_eq!(result, 5);
    }

    #[test]
    fn test_1() {
        assert_eq!(delta_year(1900), 0);
        assert_eq!(delta_year(1901), 1);
        assert_eq!(delta_year(1902), 2);
        assert_eq!(delta_year(1903), 3);
        assert_eq!(delta_year(1904), 4);
        assert_eq!(delta_year(1905), 6);
        assert_eq!(delta_year(2018), 0);
        assert_eq!(delta_year(2021), 4);
        assert_eq!(delta_year(2022), 5);
    }

    #[test]
    fn test_2021_is_not_leap_year() {
        let result = is_leap_year(2021);
        assert_eq!(result, false);
    }

    #[test]
    fn test_multiple_of_4_is_leap_year() {
        let result = is_leap_year(1996);
        assert_eq!(result, true);

        let result = is_leap_year(2020);
        assert_eq!(result, true);
    }

    #[test]
    fn test_multiple_of_100_is_not_leap_year() {
        let result = is_leap_year(2100);
        assert_eq!(result, false);

        let result = is_leap_year(1900);
        assert_eq!(result, false);
    }

    #[test]
    fn test_multiple_of_400_is_leap_year() {
        let result = is_leap_year(1600);
        assert_eq!(result, true);

        let result = is_leap_year(2000);
        assert_eq!(result, true);
    }
}
