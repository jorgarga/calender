
// This calendar example is provided by:
// http://www.codingunit.com Programming Tutorials
//
// compile
// gcc -o calender calender.c

#include<stdio.h>

int days_in_month[]={31,28,31,30,31,30,31,31,30,31,30,31};
char *months[]= {
	"\n\nJanuary",
	"\n\nFebruary",
	"\n\nMarch",
	"\n\nApril",
	"\n\nMay",
	"\n\nJune",
	"\n\nJuly",
	"\n\nAugust",
	"\n\nSeptember",
	"\n\nOctober",
	"\n\nNovember",
	"\n\nDecember"
};


int inputyear(void) {
	int year;
	
	printf("Please enter a year (example: 1999) : ");
	scanf("%d", &year);
	return year;
}

int determinedaycode(int year) {
	int daycode;
	int d1, d2, d3;
	
	d1 = (year - 1.)/ 4.0;
	d2 = (year - 1.)/ 100.;
	d3 = (year - 1.)/ 400.;
	daycode = (year + d1 - d2 + d3) %7;
	return daycode - 1;
}


void determineleapyear(int year) {
	if(year% 4 == 0 && (year%100 != 0 || year%400 == 0)) {
		days_in_month[1] = 29;
    } else {
		days_in_month[1] = 28;
	}
}

void calendar(int daycode) {
	int month, day;
    
	for ( month = 0; month < 12; month++ ) {
		printf("%s", months[month]);
		printf("\n\nMon  Tue  Wed  Thu  Fri  Sat  Sun\n" );
		
		// Correct the position for the first date
		for ( day = 0; day <= daycode * 5; day++ ) {
			printf(" ");
		}
		
		// Print all the dates for one month
		for ( day = 1; day < days_in_month[month] + 1; day++ ) {
			printf("%2d", day);
			
			if ( (day + daycode) % 7 > 0 ){
                printf("   " );
			} else {
                printf("\n " );
            }
		}
        // Set position for next month
        daycode = ( days_in_month[month] + daycode) % 7;
	}
}

int main(void) {
	int year, daycode;
	
	year = inputyear();
	daycode = determinedaycode(year);

	determineleapyear(year);
	calendar(daycode);
	printf("\n");
}

