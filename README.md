# Cal

Command line tool to print a calender on the screen.
Usage:
> calender 1999       # prints the calender for year 1999.
> 
> calender 12 1999 # prints the calender for december 1999.

I was curious about how to implement a calender and found an educative example on [CodingUnit](https://www.codingunit.com/how-to-make-a-calendar-in-c) which served as base for my project. My modified version is in the `doc` folder.


# Leap year calculation
There are different to calculate the same. I found the following approaches interesting and wanted to analyze them further.

## Classical example

```rust
fn is_leap_year(year: usize) -> bool {
    (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0)
}
```

```asm
example::is_leap_year:
        sub     rsp, 16
        mov     qword ptr [rsp], rdi
        and     rdi, 3
        cmp     rdi, 0
        jne     .LBB0_2
        mov     rax, qword ptr [rsp]
        mov     ecx, 100
        xor     edx, edx
        div     rcx
        cmp     rdx, 0
        je      .LBB0_4
        jmp     .LBB0_5
.LBB0_2:
        mov     byte ptr [rsp + 14], 0
.LBB0_3:
        mov     al, byte ptr [rsp + 14]
        and     al, 1
        movzx   eax, al
        add     rsp, 16
        ret
.LBB0_4:
        mov     rax, qword ptr [rsp]
        mov     ecx, 400
        xor     edx, edx
        div     rcx
        cmp     rdx, 0
        sete    al
        and     al, 1
        mov     byte ptr [rsp + 15], al
        jmp     .LBB0_6
.LBB0_5:
        mov     byte ptr [rsp + 15], 1
.LBB0_6:
        mov     al, byte ptr [rsp + 15]
        and     al, 1
        mov     byte ptr [rsp + 14], al
        jmp     .LBB0_3

__rustc_debug_gdb_scripts_section__:
        .asciz  "\001gdb_load_rust_pretty_printers.py"
```

## Example using match

```rust
fn is_leap_year(year: usize) -> bool {
    match (year % 400, year % 100, year % 4) {
         (0, _, _) => true,
         (_, 0, _) => false,
         (_, _, 0) => true,
         (_, _, _) => false
    }
}
```

```asm
example::is_leap_year:
        sub     rsp, 40
        mov     ecx, 400
        mov     rax, rdi
        xor     edx, edx
        div     rcx
        mov     qword ptr [rsp], rdx
        mov     ecx, 100
        mov     rax, rdi
        xor     edx, edx
        div     rcx
        mov     rax, qword ptr [rsp]
        and     rdi, 3
        mov     qword ptr [rsp + 16], rax
        mov     qword ptr [rsp + 24], rdx
        mov     qword ptr [rsp + 32], rdi
        cmp     qword ptr [rsp + 16], 0
        jne     .LBB0_2
        mov     byte ptr [rsp + 15], 1
        jmp     .LBB0_7
.LBB0_2:
        cmp     qword ptr [rsp + 24], 0
        jne     .LBB0_4
        mov     byte ptr [rsp + 15], 0
        jmp     .LBB0_7
.LBB0_4:
        cmp     qword ptr [rsp + 32], 0
        jne     .LBB0_6
        mov     byte ptr [rsp + 15], 1
        jmp     .LBB0_7
.LBB0_6:
        mov     byte ptr [rsp + 15], 0
.LBB0_7:
        mov     al, byte ptr [rsp + 15]
        and     al, 1
        movzx   eax, al
        add     rsp, 40
        ret

__rustc_debug_gdb_scripts_section__:
        .asciz  "\001gdb_load_rust_pretty_printers.py"
```

 
